import firebase from 'firebase'
import 'firebase/firestore'

const firebaseConfig = {
  apiKey: "AIzaSyC4JSE9NMM1oPLxjtaRharZer8k5qXXJHM",
  authDomain: "friendfinder-650e7.firebaseapp.com",
  databaseURL: "https://friendfinder-650e7.firebaseio.com",
  projectId: "friendfinder-650e7",
  storageBucket: "gs://friendfinder-650e7.appspot.com",
  messagingSenderId: "469740849707",
  appId: "1:469740849707:web:babc55645b27989d"
}

firebase.initializeApp(firebaseConfig)

// firebase utils
const db = firebase.firestore()
const auth = firebase.auth()
const currentUser = auth.currentUser

// firebase collections
const usersCollection = db.collection('users')

export {
  db,
  auth,
  currentUser,
  usersCollection 
}
