import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/pages/Index'
import Login from '@/pages/Login'
import SignUp from '@/pages/SignUp'
import NewFeeds from '@/pages/NewFeeds'

const fb = require('@/firebase/firebaseConfig.js')

Vue.use(Router);

const router =  new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index,
    },
    {
      path: '/sign-in',
      name: 'SignIn',
      component: Login,
    },
    {
      path: '/sign-up',
      name: 'SignUp',
      component: SignUp,
    },
    {
      path: '/newfeeds',
      name: 'NewFeeds',
      component: NewFeeds,
      meta: {
        requiresAuth: true
      }
    }
  ],
  mode: 'history'
})

router.beforeEach((to, from, next) => {
  const currentUser = fb.auth.currentUser
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)

  if (requiresAuth && !currentUser) next('sign-in')
  else if (!requiresAuth && currentUser) next('newfeeds')
  else next()
})

export default router