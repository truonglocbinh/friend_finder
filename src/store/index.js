import Vue from 'vue'
import Vuex from 'vuex'
import createState from './state'
import mutations from './mutations'
import actions from './actions'
import getters from './getters'

const fb = require('@/firebase/firebaseConfig.js')

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: createState(),
  getters,
  actions,
  mutations
})

fb.auth.onAuthStateChanged(user => {
  if (user) {
    store.commit('setCurrentUser', user)
    store.dispatch('fetchUserProfile')
  }
})
