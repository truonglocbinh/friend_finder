export default {
  currentUser: state => state.currentUser,
  userProfile: state => state.userProfile
}
